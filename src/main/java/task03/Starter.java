package task03;

import task03.strings_array.StringsArray;
import task03.strings_pair.StringsPair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class Starter {
    public static void main(String[] args) {
        testStringsArray();
        testStringsPair();
    }

    private static void testStringsArray(){
//        StringsArray<String> example_1 = new StringsArray<>();
//        example_1.addString("first");
//        example_1.addString("second");
//        example_1.addString("third");
//        System.out.println(example_1.getSize() + " " + example_1);
//        StringsArray<String> example_2 = new StringsArray<>(example_1.getStrings());
//        example_2.addString("forth");
//        example_2.addString("fifth");
//        System.out.println(example_2.getSize() + " " + example_2);
    }

    private static void testStringsPair(){
        List<StringsPair> countriesAndCapitals = new ArrayList<>();
        StringsPair sriLanka = new StringsPair("Sri Lanka", "Colombo, Sri Jayewardenepura Kotte");
        countriesAndCapitals.add(new StringsPair("Cape Verde", "Praia"));
        countriesAndCapitals.add(new StringsPair("Afghanistan", "Kabul"));
        countriesAndCapitals.add(new StringsPair("Zambia", "Lusaka"));
        countriesAndCapitals.add(new StringsPair("Bahrain", "Manama"));
        countriesAndCapitals.add(new StringsPair("Morocco", "Rabat"));
        countriesAndCapitals.add(new StringsPair("Haiti", "Port-au-Prince"));
        countriesAndCapitals.add(new StringsPair("Antigua and Barbuda", "Saint John’s"));
        countriesAndCapitals.add(sriLanka);
        Collections.sort(countriesAndCapitals);
        Consumer<StringsPair> consumer = (e) -> System.out.print(e + "; ");
        System.out.println("Sorted by country name(default soring using Comparable):");
        countriesAndCapitals.forEach(consumer);
        System.out.println();
        System.out.println();
        Comparator<StringsPair> comparatorCapitalFirst = Comparator.comparing(StringsPair::getSecond);
        countriesAndCapitals.sort(comparatorCapitalFirst);
        System.out.println("Sorted by capital name(using Comparator:");
        countriesAndCapitals.forEach(consumer);
        System.out.println();
        System.out.println();
        System.out.println("Element found using Collections.binarySearch(Collection<E> coll, E e, Comparator<E> comp): " +
                Collections.binarySearch(countriesAndCapitals, sriLanka, comparatorCapitalFirst));
    }
}

