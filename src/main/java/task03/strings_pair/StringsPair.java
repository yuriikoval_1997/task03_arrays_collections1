package task03.strings_pair;

public class StringsPair implements Comparable<StringsPair>{
    private String first;
    private String second;

    public StringsPair(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    @Override
    public int compareTo(StringsPair stringsPair) {
        return first.compareTo(stringsPair.getFirst());
    }

    @Override
    public String toString() {
        return first + " = " + second;
    }
}
