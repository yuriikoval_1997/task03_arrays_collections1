package task03.strings_array;

import java.lang.reflect.Constructor;
import java.util.Objects;
import java.util.function.Supplier;

public class StringsArray<E extends String> {
    private E[] strings;
    private Constructor constructor;
    private int indexOfLastElement = -1;

    public StringsArray(Supplier<E> supplier) throws NoSuchMethodException {
        this.constructor = supplier.get().getClass().getConstructor();
    }

    public StringsArray(E[] strings) {
        this.strings = Objects.requireNonNull(strings);
        indexOfLastElement = getIndexOfLastCell();
        //this.constructor ?????
    }

    private int getIndexOfLastCell(){
        return strings.length - 1;
    }

    private void add(E string){
        E[] temp = strings;
        strings = constructor[];
    }

    public int getSize(){
        return strings.length;
    }

    public E[] getStrings() {
        return strings;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (E e : strings){
            builder.append(e).append("; ");
        }
        return builder.toString();
    }
}
