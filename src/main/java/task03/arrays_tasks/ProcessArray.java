package task03.arrays_tasks;

import java.util.*;

public class ProcessArray {
    private int[] array;

    public ProcessArray(int[] array) {
        this.array = array;
    }

    public int[] deleteRepeatedElements(){
        LinkedHashMap<Integer, Integer> elements = new LinkedHashMap<>();
        for (int e : array){
            elements.merge(e, 1,  Integer::sum);
        }
        int numberOfRepeats = 0;
        for (Map.Entry<Integer, Integer> entry: elements.entrySet()){
            if (entry.getValue() > 2) {
                numberOfRepeats++;
            }
        }
        int[] result = new int[numberOfRepeats];
        int i = 0;
        for (Map.Entry<Integer, Integer> entry: elements.entrySet()){
            result[i] = entry.getKey();
        }
        return result;
    }

    public List<Integer> deleteSequences(){
        List<Integer> list = new ArrayList<>();
        int sequenceLength = 1;
        for (int i = 0; i < array.length - 1; i++) {
            if (sequenceLength == 1){
                list.add(array[i]);
                sequenceLength = 1;
            }
            if (array[i] == array[i + 1]){
                sequenceLength++;
            }
        }
        return list;
    }
}
