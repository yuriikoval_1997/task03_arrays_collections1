package task03.arrays_tasks;

import java.util.Arrays;
import java.util.Objects;

public class CommonElements {
    private int[] first;
    private int[] second;

    public CommonElements(int[] first, int[] second) {
        this.first = Objects.requireNonNull(first);
        this.second = Objects.requireNonNull(second);
    }

    public int[] getFirst() {
        return first;
    }

    public int[] getSecond() {
        return second;
    }

    public int[] findCommonElements(){
        boolean isSwapped = false;
        if (first.length > second.length){
            swapArrays();
            isSwapped = true;
        }
        Arrays.sort(first);
        ResizableArray resizableArray = new ResizableArray();
        for (int e : second) {
            if (Arrays.binarySearch(first, e) > 0) {
                resizableArray.add(e);
            }
        }
        if (isSwapped){
            swapArrays();
        }
        return resizableArray.trim();
    }

    public int[] findUniqueElementInFirstArray(){
        Arrays.sort(second);
        ResizableArray resizableArray = new ResizableArray();
        for (int e : first) {
            if (Arrays.binarySearch(second, e) == -1) {
                resizableArray.add(e);
            }
        }
        return resizableArray.trim();
    }

    private void swapArrays(){
        int[] temp = second;
        second = first;
        first = temp;
    }
}
