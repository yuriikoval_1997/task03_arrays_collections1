package task03.arrays_tasks;

public class ResizableArray {
    private int[] array;
    private int indexOfLastElement = -1;

    public ResizableArray(){
        this.array = new int[1];
    }

    public ResizableArray(int[] array) {
        this.array = array;
        indexOfLastElement = getIndexOfLastCell();
    }

    private int getIndexOfLastCell(){
        return array.length - 1;
    }

    public void add(int element){
        if (indexOfLastElement == getIndexOfLastCell()){
            int[] temp = array;
            array = new int[temp.length * 2];
            System.arraycopy(temp, 0, array, 0, temp.length);
            indexOfLastElement++;
            array[indexOfLastElement] = element;
        } else {
            indexOfLastElement++;
            array[indexOfLastElement] = element;
        }
    }

    public void add(int element, int i){
        if (i > getIndexOfLastCell()){
            throw new ArrayIndexOutOfBoundsException();
        } else {
            this.add(element);
        }
    }

    public int[] trim(){
        if (indexOfLastElement == getIndexOfLastCell()){
            return array;
        }
        int[] temp = array;
        array = new int[indexOfLastElement];
        System.arraycopy(temp, 0, array, 0, temp.length);
        return array;
    }
}
